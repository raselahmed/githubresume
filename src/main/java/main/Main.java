package main;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "de.github.resume.**" })
public class Main {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(Main.class, args);
	}
}
