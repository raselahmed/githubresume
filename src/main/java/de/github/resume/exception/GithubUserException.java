package de.github.resume.exception;

public class GithubUserException extends Exception {

	private static final long serialVersionUID = -2083294634264921162L;

	/**
	 * Instantiates a new GithubUser Exception.
	 */
	public GithubUserException() {
		super();
	}

	/**
	 * Instantiates a new GithubUser Exception.
	 * 
	 * @param message
	 *            is the message.
	 */
	public GithubUserException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new GithubUser Exception.
	 * 
	 * @param message
	 *            is the message.
	 * @param cause
	 *            is the cause.
	 */
	public GithubUserException(String message, Throwable cause) {
		super(message, cause);
	}

}
