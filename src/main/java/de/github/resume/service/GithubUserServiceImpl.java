package de.github.resume.service;

import java.io.IOException;
import java.util.List;

import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import de.github.resume.exception.GithubUserException;
import de.github.resume.model.GithubUser;

@Service
public class GithubUserServiceImpl implements GithubUserService {

	private static final Logger LOG = LoggerFactory.getLogger(GithubUserServiceImpl.class);

	/**
	 * get github user info by userName
	 */
	@Override
	public GithubUser getUserInfoByUserName(final String userName) throws GithubUserException {
		final GithubUser githubUser = new GithubUser();
		final RepositoryService service = new RepositoryService();
		List<Repository> repositories = null;
		try {
			repositories = service.getRepositories(userName);
			for (final Repository repo : repositories) {
				if (repo.getLanguage() != null) {
					githubUser.getProgrammingLanguages().add(repo.getLanguage());
				}
			}
			githubUser.setUserName(userName);
		} catch (IOException e) {
			LOG.error("Error during github user info retrieve.", e);
			throw new GithubUserException(e.getMessage());
		}
		return githubUser;
	}

}
