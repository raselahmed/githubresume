package de.github.resume.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.github.resume.exception.GithubUserException;
import de.github.resume.model.GithubUser;
import de.github.resume.service.GithubUserService;

/**
 * Handles requests for the GithubUser service.
 */
@RestController
public class GithubUserController {

	private static final Logger LOG = LoggerFactory.getLogger(GithubUserController.class);

	@Autowired
	private GithubUserService githubUserService;

	/**
	 * resource for git user info by user name
	 * 
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/userinfo", method = RequestMethod.GET)
	public ResponseEntity<GithubUser> getUserInfoByUserName(@RequestParam(value = "username", required = true) final String userName) {
		GithubUser githubUser = null;
		try {
			githubUser = githubUserService.getUserInfoByUserName(userName);
		} catch (GithubUserException e) {
			LOG.error(e.getMessage(), e);
			return new ResponseEntity<GithubUser>(githubUser, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<GithubUser>(githubUser, HttpStatus.OK);
	}

}
