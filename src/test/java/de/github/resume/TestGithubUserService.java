package de.github.resume;

import main.Main;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.github.resume.exception.GithubUserException;
import de.github.resume.model.GithubUser;
import de.github.resume.service.GithubUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Main.class)
@WebIntegrationTest
public class TestGithubUserService {

  @Autowired
  private GithubUserService githubUserService;

  @Before
  public void setUp() {

  }

  @Test
  public void tesTgetUserInfoByUserName() throws GithubUserException {
    final GithubUser user = githubUserService.getUserInfoByUserName("Billy");
    Assert.assertEquals(user.getUserName(), "Billy");

  }
}
