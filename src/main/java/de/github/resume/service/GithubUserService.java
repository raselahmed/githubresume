package de.github.resume.service;

import de.github.resume.exception.GithubUserException;
import de.github.resume.model.GithubUser;

public interface GithubUserService {

  GithubUser getUserInfoByUserName(String userName) throws GithubUserException;

}
