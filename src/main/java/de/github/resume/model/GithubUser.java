package de.github.resume.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Rasel Ahmed
 */
public class GithubUser implements Serializable {

	private static final long serialVersionUID = 6366120654176907487L;

	private String userName;

	private Set<String> programmingLanguages;

	/**
	 * @return Returns the userName.
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            The userName to set.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return Returns the programmingLanguages.
	 */
	public Set<String> getProgrammingLanguages() {
		if (programmingLanguages == null) {
			programmingLanguages = new HashSet<>();
		}
		return programmingLanguages;
	}

}
